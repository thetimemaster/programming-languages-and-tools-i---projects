#include <map>
#include <iostream>
#include <sstream>
#include <set>
#include <string>
#include <algorithm>
#include <vector>
#include <limits>
#include <regex>
using namespace std;

const string regexStringStop = "[[:alpha:]_\\^]+";
const string regexStringCourseID = "[\\d]+";

// Just for clarity.
using Stop = string;

// Time is a pair of hour and minute.
using Time = pair<unsigned, unsigned>;
#define tHour first
#define tMinute second

// Ticket has length price and name.
// This order in pairs is chosen for easy removal of suboptimal tickets.
using Ticket = pair<pair<unsigned, unsigned long long>, string>;
#define tLength first.first
#define tPrice first.second
#define tName second

// Course maps Stops to Times.
using Course = map<Stop, Time>;

// CourseIDs are also mapped to courses.
map<string, Course> courses;

// Tickets are just sorted.
set<Ticket> tickets;

set<string> ticketNames;

// Global for convenience.
unsigned lineNo, sumTickets;
string currentLine;

// Returns true if t is a valid time, false otherwise.
bool isValidTime(const Time &t)
{
    if ((t.tHour < 5) || (t.tHour == 5 && t.tMinute < 55) || (t.tHour > 21) || (t.tHour == 21 && t.tMinute > 21))
        return false;
    return true;
}

// Returns a number of seconds between two given Times.
int timeDifference(Time t1, Time t2)
{
    return t2.tMinute - t1.tMinute + 60 * (t2.tHour - t1.tHour);
}

// Returns true if s is a valid string defining a course, false otherwise.
bool isValidCourse(const string &s)
{
    static const regex validCourseR(
            regexStringCourseID
            + "(?: (?:\\d|1\\d|2[0-3]):[0-5]\\d "
            + regexStringStop
            + ")+"
            );
    return regex_match(s, validCourseR);
}

// Returns true if s is a valid string defining a ticket, false otherwise.
bool isValidTicket(const string &s)
{
    static const regex validTicketR("[ [:alpha:]]+ [\\d]+[\\.][\\d]{2} [1-9][\\d]*");
    return regex_match(s, validTicketR);
}

// Returns true if s is a valid string defining a query, false otherwise.
bool isValidQuery(const string &s)
{
    static const regex validQueryR(
            "\\? "
            + regexStringStop
            + "(?: "
            + regexStringCourseID
            + " "
            + regexStringStop
            + ")+"
            );
    return regex_match(s, validQueryR);
}

// Trims insignificant zeros. Assumes s is a string of digits.
void trimInsignificantZeros(string &s)
{
    string::size_type pos = s.find_first_of("123456789");
    if (pos == string::npos)
        s = "0";
    else
        s = s.substr(pos);
}

// Adds ticket to collection if viable. Removes all tickets both shorter and more expensive.
bool addTicket(unsigned length, unsigned long long price, string &name)
{
    // We have separate set for names as we remove suboptimal tickets from main structure.
    if (ticketNames.find(name) != ticketNames.end())
    return false;

    ticketNames.insert(name);

    auto stats = make_pair(length, price);

    while (true)
    {
        auto next = tickets.lower_bound(make_pair(stats, ""));

        if (next != tickets.end())
        {
            if (next->first == stats)
            {
                return true; // Ticket with same stats exists.
            }

            if (next->tLength == length) // Found ticket with same length and > in normal pair order, must be more expensive.
            {
                tickets.erase(next);
                continue;
            }

            if (next->tPrice <= price) // Found strictly longer ticket for same or cheaper.
            {
                return true; // Current ticket is not viable.
            }
        }

        if (next != tickets.begin()) // There exists strictly shorter ticket.
        {
            next--;
            if (next->tPrice >= price) // Shorter ticket is more expensive.
            {
                tickets.erase(next);
                continue;
            }
        }

        break;  // No case is matched, we can add ticket.
    }

    tickets.insert(make_pair(stats, name));
    
    return true;
}

// Tries to add course. Returns false if course with such id exists, true otherwise.
bool addCourse(string id, Course &course)
{
    if (courses.find(id) == courses.end())
    {
        courses.insert(make_pair(id, course));
        return true;
    }

    return false;
}

// Returns best tickets for the route of given time. Vector is empty if it is impossible to do in 3 tickets.
vector<Ticket> getBestTickets(unsigned time)
{
    vector<Ticket> bestTickets;
    double bestPrice = numeric_limits<unsigned long long>::max();

    // Will iterate every pair of tickets and find shortest to fill the remaining time. O(n^2log n).
    for (auto t1 = tickets.begin(); t1 != tickets.end(); t1++)
    {
        if (t1->tLength >= time) //one ticket is enough
        {
            if (t1->tPrice < bestPrice)
            {
                bestPrice = t1->tPrice;
                bestTickets.clear();
                bestTickets.push_back(*t1);
            }
            break; // All single tickets will be longer and more expensive.
        }
        else
        {
            for (auto t2 = t1; t2 != tickets.end(); t2++)
            {
                if (t1->tLength + t2-> tLength >= time) // Two tickets are enough.
                {
                    if (t1->tPrice + t2->tPrice < bestPrice)
                    {
                        bestPrice = t1->tPrice + t2->tPrice;
                        bestTickets.clear();
                        bestTickets.push_back(*t1);
                        bestTickets.push_back(*t2);
                    }
                    break; // All pairs with same first element will be longer and more expensive.
                }
                else
                {
                    // Find the cheapest ticket to fill the remaining time.
                    auto t3 = tickets.lower_bound(make_pair(make_pair(time - t1->tLength - t2->tLength, 0), ""));

                    if (t3 != tickets.end())
                    {
                        if (t1->tPrice + t2->tPrice + t3->tPrice < bestPrice)
                        {
                            bestPrice = t1->tPrice + t2->tPrice + t3->tPrice;
                            bestTickets.clear();
                            bestTickets.push_back(*t1);
                            bestTickets.push_back(*t2);
                            bestTickets.push_back(*t3);
                        }
                    }
                }
            }
        }
    }
    return bestTickets;
}

// Signals error in current line in correct format.
void answerErrorLine()
{
    cerr << "Error in line " << lineNo << ": " << currentLine << endl;
}

// Prints that you would have to wait on given stop in correct format.
void answerWaitingLine(Stop where)
{
    cout << ":-( " << where << endl;
}

//
void answerImpossibleLine()
{
    cout << ":-|" << endl;
}

// Prints given tickets in correct format.
void answerOkLine(vector<Ticket> ticketsToBuy)
{
    sumTickets += ticketsToBuy.size();

    cout << "!";

    for (size_t i = 0; i < ticketsToBuy.size(); i++)
    {
        cout << " " << ticketsToBuy[i].tName;

        if (i < ticketsToBuy.size() - 1)
        {
            cout << ";";
        }
        else
        {
            cout << endl;
        }
    }
}

// Answers given query, stopNames must have exactly one more element than courseIds. Returns true if input is correct,
// false otherwise.
bool answer(vector<string> &courseIDs, vector<Stop> &stopNames)
{
    if (courseIDs.size() == 0) return false;

    Time firstStopTime;
    Time lastStopTime;
    int hadToWaitIndex = -1;

    for (size_t i = 0; i < courseIDs.size(); i++)
    {
        auto mapCheck = courses.find(courseIDs[i]);

        // Course does not exist.
        if (mapCheck == courses.end())
        {
            return false;
        }

        Course& course = mapCheck->second;

        auto leaveTime = course.find(stopNames[i]);
        auto arriveTime = course.find(stopNames[i + 1]);

        // Stops don't exist on course or they are in wrong order.

        if (leaveTime == course.end() || arriveTime == course.end() || leaveTime->second > arriveTime->second)
        {
            return false;
        }

        if (i > 0 && leaveTime->second < lastStopTime)
        {
            return false;
        }

        if (i > 0 && leaveTime->second > lastStopTime)
        {
            hadToWaitIndex = i;
            break;
        }

        if (i == 0) firstStopTime = leaveTime->second;
        lastStopTime = arriveTime->second;
    }

    if (hadToWaitIndex != -1) // Has to wait somewhere.
    {
        answerWaitingLine(stopNames[hadToWaitIndex]);
    }
    else
    {
        int journeyTime = timeDifference(firstStopTime, lastStopTime) + 1;

        vector<Ticket> ticketsToBuy = getBestTickets(journeyTime);

        if(ticketsToBuy.empty())
        {
            answerImpossibleLine();
        }
        else
        {
            answerOkLine(ticketsToBuy);
        }
    }
    return true;
}

// Handles one line of input. Returns true if the line is correct, false otherwise.
bool nextLine(const string &line)
{
    if (isValidCourse(line))
    {
        string formattedString = line;
        string::size_type pos;
        while ((pos = formattedString.find(":")) != string::npos)
        {
            formattedString.replace(pos, 1, " ");
        }
        istringstream iss(formattedString);

        Course course;
        string courseID;
        Time time, previousTime;
        Stop stopName;

        iss >> courseID;
        trimInsignificantZeros(courseID);

        while (iss >> time.tHour)
        {
            iss >> time.tMinute >> stopName;
            
            if (!isValidTime(time)) return false;
            if (previousTime >= time) return false;
            if (course.find(stopName) != course.end()) return false;
            
            course[stopName] = time;
            previousTime = time;
        }
        return addCourse(courseID, course);
    }
    else if (isValidTicket(line))
    {
        unsigned nameLength = line.find_first_of("0123456789") - 1;
        string ticketName = line.substr(0, nameLength);
        string substring = line.substr(nameLength + 1);
        substring.replace(substring.find("."), 1, "");
        istringstream iss(substring);

        unsigned long long price;
        unsigned length;

        iss >> price >> length;
        return addTicket(length, price, ticketName);
    }
    else if (isValidQuery(line))
    {
        string formattedString = line.substr(2);
        istringstream iss(formattedString);
        vector<string> courseIDs;
        vector<Stop> stopNames;
        string courseID;
        Stop curStop;

        iss >> curStop;
        stopNames.push_back(curStop);

        while (iss >> courseID)
        {
            iss >> curStop;
            trimInsignificantZeros(courseID);
            courseIDs.push_back(courseID);
            stopNames.push_back(curStop);
        }
        return answer(courseIDs, stopNames);
    }
    else if (line.empty())
    {
        return true;
    }
    else
    {
        return false;
    }
}

int main()
{
    while (getline(cin, currentLine))
    {
        if (!cin)
        {
            return 0;
        }
        lineNo++;
        if (!nextLine(currentLine))
        {
            answerErrorLine();
        }
    }
    cout << sumTickets << "\n";
}